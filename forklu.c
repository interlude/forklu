#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <getopt.h>
#include <sys/mman.h>
#include <signal.h>




#define PARENT_PIPE 1
#define CHILD_PIPE 0
#define TRUE 1
#define FALSE 0

int debugMode = FALSE;
struct processInfo{
	pid_t pid;		
	char full;		//dolu mu değil mi

};
struct connectionInfo
{
	struct sockaddr_in clientaddr;
	int sockDesc;
};
int portno = 0;
int processCount = 0;
char* portString = "";
pid_t parent;// = getpid();

int openSocket();
void createChildProcess(int);
void initServerAddr(struct sockaddr_in servaddr);
void error(const char *msg);
void getOptions(int argc, char* argv[]);
void bindSocket(struct sockaddr_in servadd, int sock);
void childFunction();
int waitChild();

struct processInfo *processes = NULL; //proses bilgileri dizisi
int sockfd;
int main(int argc, char *argv[])
{

	
	
	
	//int j;
	pid_t freeChild;


	struct sockaddr_in serv_addr;
	
		



	parent = getpid();
	getOptions(argc,argv);
	//initServerAddr(serv_addr);

	

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(portno);
	if (bind(sockfd, (struct sockaddr *) &serv_addr,
		sizeof(serv_addr)) < 0) 
		error("ERROR on binding");

	if(debugMode)
	printf("Creating %d processes...\n", processCount);
	
	createChildProcess(processCount);

	//sockfd = openSocket();
	//bindSocket(serv_addr,sockfd);
	
	while(1){
	printf("Waiting request...\n");
	listen(sockfd,5);
	
	freeChild = waitChild();
	kill(freeChild, SIGUSR2);
	}


	close(sockfd);
	return 0; 
}

int isChild(pid_t pid){
int j = 0;
while(j<processCount){
	printf("Looking to process with index %d \n",j );
	if(processes[j].pid == pid)
		return TRUE;
	j++;
}
return FALSE;

}


void waitParent(){
	sigset_t waitset;
    siginfo_t info;
    int result;

    sigemptyset( &waitset );
    sigaddset( &waitset, SIGUSR2 );
    sigprocmask(SIG_BLOCK, &waitset, NULL);


   

   result = sigwaitinfo( &waitset, &info );
   printf("Result  %d\n",result);
   if( result != -1){
   		printf("Pid of sender = %d\n",info.si_pid);
   		printf("Pid of reciever = %d\n",getpid());
        if(parent == info.si_pid){
        	printf("%s\n", "Parent konustu!!\n");
        }
    }
}

pid_t waitChild(){
	
    sigset_t waitset;
    siginfo_t info;
    int result;

    sigemptyset( &waitset );
    sigaddset( &waitset, SIGUSR1 );
    sigprocmask(SIG_BLOCK, &waitset, NULL);


   

   result = sigwaitinfo( &waitset, &info );
   printf("Result  %d\n",result);
   if( result != -1){
   		printf("Pid of sender = %d\n",info.si_pid);
        if(isChild(info.si_pid)){
        	printf("%s\n", "Cocuk konustu!!\n");
        }
    }

    return info.si_pid;
    
}



void createChildProcess(int processCount)
{
	int i;

	pid_t lastProcessID = getpid();
	if(debugMode)
	printf("ProcessID= %d\n", lastProcessID);
	

	processes = (struct processInfo*) malloc( (sizeof(struct processInfo))  * (processCount+10) ); //just in case +10
	if(processes == NULL)
	{
		perror("malloc pid array");
		exit(-1);
	}


	i = 0;
	while(i<=processCount && lastProcessID != 0){
		lastProcessID = fork();
		if(lastProcessID!=0){//parent
			if(lastProcessID!=-1){
				processes[i].pid = lastProcessID;
				i++;
			}
			else{
				perror("Process creation fault");
				exit(-1);
			}
		}

	}

	if(lastProcessID){//parent

		
		printf("ProcessIDs:\n");

		for (i = 0; i < processCount; i ++)
		{
			printf("Process %d, pid= %d \n",i+1, processes[i].pid );
		}

		fflush(stdout);

		
	}
	else{
		childFunction();
	}
	

}



void childFunction(){

	socklen_t clilen;
	char buffer[256];
	int newsockfd;
	int n;
	struct sockaddr_in cli_addr;
	while(1){
	sleep(3);
	kill(parent, SIGUSR1);
	waitParent();
	clilen = sizeof(cli_addr);
	
	printf("sockfd = %d\n", sockfd);
	newsockfd = accept(sockfd, 
		(struct sockaddr *) &cli_addr, 
		&clilen);
	if(debugMode)
	printf("Request got by process with id %d\n", getpid());
	
	if (newsockfd < 0) 
		error("ERROR on accept");
	bzero(buffer,256);
	n = read(newsockfd,buffer,255);
	if (n < 0) error("ERROR reading from socket");
	printf("Here is the message: %s\n",buffer);
	n = write(newsockfd,"I got your message",18);
	if (n < 0) error("ERROR writing to socket");
	close(newsockfd);
	printf("Process with pid %d exits\n",getpid());
	}
	exit(0);

}


void printusage(){
	char* help1 = "forklu --port portno --pcnt processCount [--verbose, --help]\n";
	printf("%s",help1);
}
void getOptions(int argc, char* argv[]){
	int c;

	while (1)
	{
		static struct option long_options[] =
		{

			{"port",    required_argument, 0, 'a'},
			{"pcnt",    required_argument, 0, 'b'},
			{"verbose",    required_argument, 0, 'c'},
			{0, 0, 0, 0}
		};
           /* getopt_long stores the option index here. */
		int option_index = 0;

		c = getopt_long (argc, argv, "a:b:c",
			long_options, &option_index);

           /* Detect the end of the options. */
		if (c == -1)
			break;

		switch (c)
		{
			case 0:
               /* If this option set a flag, do nothing else now. */
			if (long_options[option_index].flag != 0)
				break;
			printf ("option %s", long_options[option_index].name);
			if (optarg)
				printf (" with arg %s", optarg);
			printf ("\n");
			break;

			case 'a':
			portno=atoi(optarg);
			portString = optarg;
			break;

			case 'b':
			printf ("Process pool: '%s'\n", optarg);
			processCount = atoi(optarg);

			break;

			case 'c':
			debugMode =1;

			break;

			case '?':
               /* getopt_long already printed an error message. */
			break;

			default:
			printusage();
			abort();
		}
	}

	if(!portno || !processCount){
		printusage();
		exit(-1);
	}
}


void error(const char *msg)
{
	perror(msg);
	exit(1);
}